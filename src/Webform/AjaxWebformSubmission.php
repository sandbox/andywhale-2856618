<?php

namespace Drupal\webform_ajax\Webform;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformThirdPartySettingsManager;
use Drupal\webform\Entity\Webform;

/**
 * Alter the webform submission form to add Ajax functionality.
 */
class AjaxWebformSubmission {

  const WEBFORM_AJAX_INLINE = 'inline';

  /**
   * Injected from service container.
   *
   * @var Drupal\webform\WebformThirdPartySettingsManager
   */
  protected $thirdPartySettingManager;

  /**
   * Dependency injection contructor.
   */
  public function __construct(WebformThirdPartySettingsManager $thirdPartySettingManager) {
    $this->thirdPartySettingManager = $thirdPartySettingManager;
  }

  /**
   * Alter the webform submission form.
   *
   * Adds a wrapper around the form and injects the ajax form submit handler.
   */
  public function formAlter(array &$form, FormStateInterface $formState, $formId) {
    $webformSubmission = $formState->getFormObject()->getEntity();
    if ($this->isAvailable($webformSubmission->getWebform())) {
      $ajax = $this->getAjaxProperties($formId);
      $form['#prefix'] = '<div id="' . $ajax['wrapper'] . '">';
      $form['#suffix'] = '</div>';
      $form['actions']['submit']['#submit'] = ['::ajaxFormSubmitHandler'];
      $form['actions']['submit']['#ajax'] = $ajax;
    }
  }

  /**
   * Check if Ajax is available (both enabled and supported) on this webform.
   *
   * @return bool
   *   If the webform has ajax available.
   */
  public function isAvailable(Webform $webform) {
    return ($this->isSupported($webform) && $this->isEnabled($webform));
  }

  /**
   * Check if Ajax is supported on this webform.
   *
   * @return bool
   *   If the webform has ajax supported.
   */
  public function isSupported(Webform $webform) {
    return ($webform->getSetting('confirmation_type') == self::WEBFORM_AJAX_INLINE);
  }

  /**
   * Check if Ajax is enabled on this webform.
   *
   * @return bool
   *   If the webform has ajax enabled.
   */
  public function isEnabled(Webform $webform) {
    return $this->thirdPartySettingManager->getThirdPartySetting('webform_ajax', 'webform_ajax') ?:
      $webform->getThirdPartySetting('webform_ajax', 'webform_ajax');
  }

  /**
   * Get the array of Ajax properties.
   *
   * @return array
   *   Controllers and selectors for use with the Ajax functionality.
   */
  protected function getAjaxProperties($formId) {
    return [
      'callback' => '\Drupal\webform_ajax\Controller\WebformAjaxController::ajaxSubmission',
      'wrapper' => $formId . '_wrapper',
      'selector' => '#' . str_replace('_', '-', $formId) . ' button[type="submit"]',
    ];
  }

}
