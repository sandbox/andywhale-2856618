<?php

namespace Drupal\webform_ajax\Webform;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformMessageManagerInterface;
use Drupal\webform\WebformSubmissionForm;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\Entity\Webform;
use Drupal\webform_ajax\Exception\WebformAjaxException;

/**
 * Processes a webform and returns an inline submission message.
 */
class AjaxWebform {

  /**
   * Injected from service container.
   *
   * @var Drupal\webform\WebformMessageManagerInterface
   */
  protected $messageManger;

  /**
   * The webform submission form.
   *
   * @var Drupal\webform\WebformSubmissionForm
   */
  protected $webformSubmissionForm;

  /**
   * Dependency injection contructor.
   */
  public function __construct(WebformMessageManagerInterface $messageManager) {
    $this->messageManager = $messageManager;
  }

  /**
   * Is Ajax functionality supported on this webform.
   */
  public function isSupported() {

  }

  /**
   * Is Ajax functionality enabled on this webform.
   */
  public function isEnabled() {

  }

  /**
   * Public method to carry out the submission.
   */
  public function submit(array $form, FormStateInterface $formState) {
    $this->submitWebform($form, $formState);
    return $this;
  }

  /**
   * Get the webform submission confirmation message as a renderable array.
   *
   * @return array
   *   A renderable array containing the webform submission confirmation string.
   */
  public function getRenderableMessage() {
    return [
      '#theme' => 'webform_ajax_message',
      '#message' => $this->getMessage(),
    ];
  }

  /**
   * Get the webform submission confirmation message.
   *
   * @return string
   *   Webform submission as string confirmation message.
   */
  public function getMessage() {
    $webformSubmission = $this->getWebformSubmissionFromForm();
    $this->messageManager->setWebformSubmission($webformSubmission);
    $this->messageManager->setWebform($this->getWebformFromSubmission($webformSubmission));
    return $this->messageManager->build(WebformMessageManagerInterface::SUBMISSION_CONFIRMATION);
  }

  /**
   * Submit the webform submission.
   */
  protected function submitWebform(array $form, FormStateInterface $formState) {
    $this->setWebformSubmissionForm($formState->getFormObject());
    $this->webformSubmissionForm->validateForm($form, $formState);
    $this->checkForErrors($formState);
    $this->webformSubmissionForm->submitForm($form, $formState);
    $this->checkForErrors($formState);
    $this->webformSubmissionForm->save($form, $formState);
  }

  /**
   * Set the webform submission form.
   */
  protected function setWebformSubmissionForm(WebformSubmissionForm $webformSubmissionForm) {
    $this->webformSubmissionForm = $webformSubmissionForm;
  }

  /**
   * Check for any errors on the webform.
   *
   * @throws Drupal\webform_ajax\Exception\WebformAjaxException
   */
  protected function checkForErrors(FormStateInterface $formState) {
    if ($formState->hasAnyErrors()) {
      throw new WebformAjaxException('Unable to successfully submit your webform');
    }
  }

  /**
   * Get the webform submission entity from the form object.
   *
   * @return Drupal\webform\Entity\WebformSubmission
   *   The users submitted webform submission.
   *
   * @throws Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  protected function getWebformSubmissionFromForm() {
    $webformSubmission = $this->webformSubmissionForm->getEntity();
    if ($webformSubmission && $webformSubmission instanceof WebformSubmission) {
      return $webformSubmission;
    }
    throw new NotFoundHttpException();
  }

  /**
   * Get the webform submission entity from the form object.
   *
   * @param Drupal\webform\Entity\WebformSubmission $webformSubmission
   *   The webform submission submitted.
   *
   * @return Drupal\webform\Entity\Webform
   *   The webform that the submission came from.
   *
   * @throws Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  protected function getWebformFromSubmission(WebformSubmission $webformSubmission) {
    $webform = $webformSubmission->getWebform();
    if ($webform && $webform instanceof Webform) {
      return $webform;
    }
    throw new NotFoundHttpException();
  }

}
