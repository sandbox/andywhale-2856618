<?php

namespace Drupal\webform_ajax\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\PrependCommand;
use Drupal\Core\Ajax\AfterCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform_ajax\Exception\WebformAjaxException;

/**
 * Defines a controller to load a view via AJAX.
 */
class WebformAjaxController {

  /**
   * Submits webform request via AJAX.
   *
   * @return \Drupal\webform_ajax\Ajax\WebformAjaxResponse
   *   The view response as ajax response.
   */
  public function ajaxSubmission(array $form, FormStateInterface $formState) {
    try {
      $response = new AjaxResponse();
      $wrapper = '#' . $formState->getBuildInfo()['form_id'] . '_wrapper';
      $response->addCommand(new RemoveCommand("{$wrapper} .js-webform-error-list"));
      $response->addCommand(new ReplaceCommand("{$wrapper}", \Drupal::Service('ajax.webform')->submit($form, $formState)->getRenderableMessage()));
      \Drupal::moduleHandler()->alter('webform_ajax_response', $response, $formState);
    }
    catch (WebformAjaxException $e) {
      $response->addCommand(new RemoveCommand("{$wrapper} .js-webform-messages"));
      $response->addCommand(new PrependCommand($wrapper, $e->getRenderableMessage()));
      foreach ($formState->getErrors() as $elementName => $error) {
        $fieldError = ['#theme' => 'webform_ajax_field_error', '#error' => ['#markup' => $error]];
        $response->addCommand(new AfterCommand("{$wrapper} [name='{$elementName}']", $fieldError));
      }
      // Clear the messages as we are sending back errors in the ajax response.
      drupal_get_messages();
    }

    return $response;
  }

}
