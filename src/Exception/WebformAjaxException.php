<?php

namespace Drupal\webform_ajax\Exception;

/**
 * Thrown when a webform fails to validat or submit.
 */
class WebformAjaxException extends \Exception {

  /**
   * Constructs a WebformAjaxException object.
   *
   * @param string $message
   *   The message for the exception.
   */
  public function __construct($message = NULL) {
    parent::__construct($message);
  }

  /**
   * Get a render array of the webform ajax error.
   *
   * @return array
   *   Renderable array containing the error message.
   */
  public function getRenderableMessage() {
    return [
      '#theme' => 'webform_ajax_error',
      '#error' => ['#markup' => $this->getMessage()],
    ];
  }

}
